# 28 March 2022 {-}

**Number of hours**: 2 hours organising slides for conference

## What was the activity? {-}

Prepare slides to guide attendees of what will happen next.

## What did I learn? {-}

Keep the information short and direct is important.

## How will this impact my future practice? {-}

I will respect the size limit of information in the slides.

## What forms of evidence did I produce? {-}

PDF with the slides.

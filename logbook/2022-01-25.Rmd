# 25 January 2022 {-}

**Number of hours**: 2 hours of class plus 1 hour of tutorial

## What was the activity? {-}

I practised my 3-minutes speech
talking about scissors.
And worked together with the class to split us into groups.

## What did I learn? {-}

I learnt that I have a minimum level of extroversion
and
I able to stay engaged in a short conversation about any topic.
And I have a dominant personality during meetings.

## How will this impact my future practice? {-}

I will be more careful to not over dominate the meetings.

## What forms of evidence did I produce? {-}

A list of the groups.
